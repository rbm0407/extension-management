<?php
$lang['lang_dir'] = 'portuguese';
$lang['lang_name'] = 'Português';
$lang['lang_code'] = 'pt-BR';
$lang['def_title'] = 'Gerenciador de Ramais';
$lang['brand'] = 'Ramais';
$lang['ext_plural'] = 'Ramais';
$lang['ext_singular'] = 'Ramal';
$lang['dep_plural'] = 'Departamentos';
$lang['dep_singular'] = 'Departamento';
$lang['emp_plural'] = 'Funcionários';
$lang['emp_singular'] = 'Funcionário';
$lang['num_plural'] = 'Números';
$lang['num_singular'] = 'Número';
$lang['loading'] = 'Carregando';
$lang['js_required'] = 'É Necessário ter Javascript para ver este conteúdo';
$lang['search'] = 'Pesquisar';
$lang['search_dep'] = 'Pesquisar por Departamento';
$lang['search_input_placeholder'] = 'Pesquisar por departamentos, nomes, números de ramais e etc...';
$lang['dep_name'] = 'Nome do Departamento';
$lang['dep_all'] = 'selecionar todos departamentos';