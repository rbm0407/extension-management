<?php

class MY_Controller extends CI_Controller 
{

	public $dataView = array();

	public function __construct()
	{
		parent::__construct();

		if ($this->uri->segment(1) == 'admin') {

			$this->dataView['title'] = 'Admin';
			
			if (!isset($_SESSION['logged'])) {
				if($this->uri->segment(2) !== 'login') {
					redirect('admin/login');
				}
			}
			else {
				$this->dataView['currentPage'] = $this->uri->segment(2);
				$this->dataView['user'] = $_SESSION['user'];
			}
		}
                
		if(isset($_SESSION['lang'])) {
			$this->lang->is_loaded = array();
			$this->lang->language = array();
			$this->lang->load('app', $_SESSION['lang']);
		}

		$this->dataView['lang_active'] = $this->lang->line('lang_code');
		$this->dataView['title'] = $this->lang->line('def_title');
		$this->dataView['currentController'] = $this->uri->segment(1);

	}

	public function bladeView($page, $data=NULL){
		echo $this->blade->view()->make($page, $data)->render();
	}

}