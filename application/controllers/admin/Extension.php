<?php

class Extension extends MY_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Department_model', 'dep');
		$this->load->model('Employee_model', 'emp');
		$this->load->model('Extension_model', 'ext');
	}

	public function index()
	{
		$this->dataView['departments'] = $this->dep->get_all();
		$this->dataView['employees'] = $this->emp->get_all();
		$this->dataView['extensions'] = $this->ext->with('department')->with('employee')->get_all();
		$this->bladeView('admin/extension_new', $this->dataView);
	}

	public function new_post()
	{
		if(!$this->input->post() || !$this->input->post('dep_id') || !$this->input->post('emp_id') || !$this->input->post('ext_num'))
		{
			$this->load->view('errors/html/error_general', array('heading' => 'Error', 'message' => 'Required fields are empty'));
			return;
		}

		$existentNumber = $this->ext->get_by('ext_num', $this->input->post('ext_num'));

		if($existentNumber)
		{
			$this->load->view('errors/html/error_general', array('heading' => 'Error', 'message' => 'This number are used by ...'));
			return;
		}

		$ins = $this->ext->insert(array(
			'dep_id' => $this->input->post('dep_id'),
			'emp_id' => $this->input->post('emp_id'),
			'ext_num' => $this->input->post('ext_num')
		));

		if($ins){
			$this->dataView['alertType'] = 'success';
			$this->dataView['message'] = 'Extension created with success';
			$this->bladeView('admin/alert', $this->dataView);
		}

	}

	public function edit($extID)
	{
		
		$ext = $this->ext->get($extID);

		if(!$ext){
			$this->load->view('errors/html/error_general', array('heading' => 'Error', 'message' => 'Extension not found'));
			return;
		}

		$this->dataView['ext'] = $ext;
		$this->dataView['departments'] = $this->dep->get_all();
		$this->dataView['employees'] = $this->emp->get_all();
		$this->session->set_flashdata('ext_id', $ext->ext_id);
		$this->bladeView('admin/extension_edit', $this->dataView);

	}

	public function edit_post()
	{

		$extID = $this->session->flashdata('ext_id');

		if(!$this->input->post() || !$this->input->post('dep_id') || !$this->input->post('emp_id') || !$this->input->post('ext_num') || !$extID)
		{
			$this->load->view('errors/html/error_general', array('heading' => 'Error', 'message' => 'Required fields are empty'));
			return;
		}

		$existentNumber = $this->ext->get_by('ext_num', $this->input->post('ext_num'));

		if($existentNumber)
		{
			if($existentNumber->ext_id != $extID)
			{
				$this->load->view('errors/html/error_general', array('heading' => 'Error', 'message' => 'This number are used by ...'));
				return;
			}
		}

		$upd = $this->ext->update($extID, array(
			'dep_id' => $this->input->post('dep_id'),
			'emp_id' => $this->input->post('emp_id'),
			'ext_num' => $this->input->post('ext_num')
		));

		if(!$upd)
		{
			$this->load->view('errors/html/error_general', array('heading' => 'Internal Error', 'message' => 'Please, contact your support'));
			return;
		}

		$this->dataView['alertType'] = 'success';
		$this->dataView['message'] = 'Extension updated';
		$this->bladeView('admin/alert', $this->dataView);

	}

	public function delete($extID)
	{
		
		$ext = $this->ext->get($extID);

		if(!$ext)
		{
			$this->load->view('errors/html/error_general', array('heading' => 'Error', 'message' => 'Extension not found'));
			return;
		}

		$this->dataView['extension'] = $ext;
		$this->session->set_flashdata('ext_id', $ext->ext_id);
		$this->bladeView('admin/extension_delete_confirm', $this->dataView);

	}

	public function delete_post()
	{
		$extID = $this->session->flashdata('ext_id');

		if(!$extID)
		{
			$this->load->view('errors/html/error_general', array('heading' => 'Internal Error', 
				'message' => 'Please, contact your support'));
			return;
		}

		$del = $this->ext->delete($extID);

		if(!$del)
		{
			$this->load->view('errors/html/error_general', array('heading' => 'Internal Error', 
				'message' => 'Please, contact your support'));
			return;
		}

		$this->dataView['alertType'] = 'success';
		$this->dataView['message'] = 'Extension deleted';
		$this->bladeView('admin/alert', $this->dataView);

	}

}