<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('User_model', 'user');
    }
    
    public function index()
    {
        redirect('admin/user/profile');
    }
    
    public function profile()
    {
        $this->dataView['userdb'] = $this->user->get($_SESSION['user']->user_id);
        $this->bladeView('admin.user_profile', $this->dataView);
    }
    
    public function edit()
    {
        if(!$this->input->post())
        {
            $this->load->view('errors/html/error_general', array('heading' => 'Error', 'message' => "No Data to retrieve"));
            return;
        }
        
        $this->form_validation->set_rules('user_name', 'User Name', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('user_login', 'User Login', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('user_email', 'User E-mail', 'required|valid_email');
        
        if($this->input->post('new_password'))
        {
            $this->form_validation->set_rules('new_password', 'User Password', 'required|min_length[6]|matches[new_password_confirm]');
            $this->form_validation->set_rules('new_password_confirm', 'User Password Confirm', 'required');
        }
        
        
        if($this->form_validation->run()==FALSE)
        {
            $this->profile();
            return;
        }
        
        $newUser = $this->input->post();
        
        if($this->input->post('new_password'))
        {
            $cryptPassword = password_hash($newUser['new_password'], PASSWORD_DEFAULT);
            $newUser['user_password'] = $cryptPassword;   
        }
        
        unset($newUser['new_password']);
        unset($newUser['new_password_confirm']);
        
        $upd = $this->user->update($_SESSION['user']->user_id, $newUser);
        
        if(!$upd){
            $this->dataView['alertType'] = 'danger';
            $this->dataView['message'] = 'Internal Error, please contact the administrator';
            $this->bladeView('admin/alert', $this->dataView);
            return;
        } else {
            $this->dataView['alertType'] = 'success';
            $this->dataView['message'] = 'User Info updated. Please logout and login again to take effect';
            $this->bladeView('admin/alert', $this->dataView);
        }
        
    }
    
}