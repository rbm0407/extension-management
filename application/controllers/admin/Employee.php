<?php

class Employee extends MY_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Employee_model', 'emp');
	}

	public function index()
	{
		$this->dataView['employees'] = $this->emp->get_all();
		$this->bladeView('admin/employee_new', $this->dataView);
	}

	public function new_post()
	{
		if(!$this->input->post() || !$this->input->post('emp_name'))
		{
			$this->load->view('errors/html/error_general', array('heading' => 'Error', 'message' => 'Required fields are empty'));
			return;
		}

		$ins = $this->emp->insert(array('emp_name' => $this->input->post('emp_name')));

		if($ins){
			$this->dataView['alertType'] = 'success';
			$this->dataView['message'] = 'Employee created with success';
			$this->bladeView('admin/alert', $this->dataView);
		}

	}

	public function edit($empID)
	{
		
		$dep = $this->emp->get($empID);

		if(!$dep){
			$this->load->view('errors/html/error_general', array('heading' => 'Error', 'message' => 'Employee not found'));
			return;
		}

		$this->dataView['employee'] = $dep;
		$this->session->set_flashdata('emp_id', $dep->emp_id);
		$this->bladeView('admin/employee_edit', $this->dataView);

	}

	public function edit_post()
	{

		$empID = $this->session->flashdata('emp_id');

		if(!$this->input->post() || !$this->input->post('emp_name') || !$empID)
		{
			$this->load->view('errors/html/error_general', array('heading' => 'Error', 'message' => 'Required fields are empty'));
			return;
		}

		$upd = $this->emp->update($empID, array('emp_name' => $this->input->post('emp_name')));

		if(!$upd)
		{
			$this->load->view('errors/html/error_general', array('heading' => 'Internal Error', 'message' => 'Please, contact your support'));
			return;
		}

		$this->dataView['alertType'] = 'success';
		$this->dataView['message'] = 'Employee updated';
		$this->bladeView('admin/alert', $this->dataView);

	}

	public function delete($empID)
	{
		
		$dep = $this->emp->get($empID);

		if(!$dep)
		{
			$this->load->view('errors/html/error_general', array('heading' => 'Error', 'message' => 'employee not found'));
			return;
		}

		$this->dataView['employee'] = $dep;
		$this->session->set_flashdata('emp_id', $dep->emp_id);
		$this->bladeView('admin/employee_delete_confirm', $this->dataView);

	}

	public function delete_post()
	{
		$empID = $this->session->flashdata('emp_id');

		if(!$empID)
		{
			$this->load->view('errors/html/error_general', array('heading' => 'Internal Error', 
				'message' => 'Please, contact your support'));
			return;
		}

		$del = $this->emp->delete($empID);

		if(!$del)
		{
			$this->load->view('errors/html/error_general', array('heading' => 'Internal Error', 
				'message' => 'Please, contact your support'));
			return;
		}

		$this->dataView['alertType'] = 'success';
		$this->dataView['message'] = 'Employee deleted';
		$this->bladeView('admin/alert', $this->dataView);

	}

}