<?php

class Login extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('User_model', 'user');
	}

	public $viewData = array('title' => 'Login Page');

	public function index()
	{
		echo $this->blade->view()->make('login/login_page', $this->viewData)->render();
	}

	public function login()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules([
			[
				'label' => 'Username',
				'field' => 'username',
				'rules' => 'required|min_length[3]'
			],
			[
				'label' => 'Password',
				'field' => 'password',
				'rules' => 'required|min_length[6]'
			],
		]);

		if ($this->form_validation->run() === false) {
			show_error($this->form_validation->error_string());
		}

		$this->db->limit(1);
		$qryUser = $this->user->get_by(['user_login' => $_POST['username']]);

		if (!$qryUser) {
			show_error('User or password are incorrect');
		}

		$qryPass = password_verify($this->input->post('password'), $qryUser->user_password);

		if ($qryPass) {
			$_SESSION['logged'] = true;
			$_SESSION['user']	= $qryUser;
			redirect('admin/home');
		}

		show_error('Invalid Username or password');

	}

	public function logout()
	{
		session_destroy();
		redirect('admin/login');
	}
}