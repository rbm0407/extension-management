<?php

class Department extends MY_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Department_model', 'dep');
	}

	public function index()
	{
		$this->dataView['departments'] = $this->dep->get_all();
		$this->bladeView('admin/department_new', $this->dataView);
	}

	public function new_post()
	{
		if(!$this->input->post() || !$this->input->post('dep_name'))
		{
			$this->load->view('errors/html/error_general', array('heading' => 'Error', 'message' => 'Required fields are empty'));
			return;
		}

		$ins = $this->dep->insert(array('dep_name' => $this->input->post('dep_name')));

		if($ins){
			$this->dataView['alertType'] = 'success';
			$this->dataView['message'] = 'Department created with success';
			$this->bladeView('admin/alert', $this->dataView);
		}

	}

	public function edit($depID)
	{
		
		$dep = $this->dep->get($depID);

		if(!$dep){
			$this->load->view('errors/html/error_general', array('heading' => 'Error', 'message' => 'Department not found'));
			return;
		}

		$this->dataView['department'] = $dep;
		$this->session->set_flashdata('dep_id', $dep->dep_id);
		$this->bladeView('admin/department_edit', $this->dataView);

	}

	public function edit_post()
	{

		$depID = $this->session->flashdata('dep_id');

		if(!$this->input->post() || !$this->input->post('dep_name') || !$depID)
		{
			$this->load->view('errors/html/error_general', array('heading' => 'Error', 'message' => 'Required fields are empty'));
			return;
		}

		$upd = $this->dep->update($depID, array('dep_name' => $this->input->post('dep_name')));

		if(!$upd)
		{
			$this->load->view('errors/html/error_general', array('heading' => 'Internal Error', 'message' => 'Please, contact your support'));
			return;
		}

		$this->dataView['alertType'] = 'success';
		$this->dataView['message'] = 'Department updated';
		$this->bladeView('admin/alert', $this->dataView);

	}

	public function delete($depID)
	{
		
		$dep = $this->dep->get($depID);

		if(!$dep)
		{
			$this->load->view('errors/html/error_general', array('heading' => 'Error', 'message' => 'Department not found'));
			return;
		}

		$this->dataView['department'] = $dep;
		$this->session->set_flashdata('dep_id', $dep->dep_id);
		$this->bladeView('admin/department_delete_confirm', $this->dataView);

	}

	public function delete_post()
	{
		$depID = $this->session->flashdata('dep_id');

		if(!$depID)
		{
			$this->load->view('errors/html/error_general', array('heading' => 'Internal Error', 
				'message' => 'Please, contact your support'));
			return;
		}

		$del = $this->dep->delete($depID);

		if(!$del)
		{
			$this->load->view('errors/html/error_general', array('heading' => 'Internal Error', 
				'message' => 'Please, contact your support'));
			return;
		}

		$this->dataView['alertType'] = 'success';
		$this->dataView['message'] = 'Department deleted';
		$this->bladeView('admin/alert', $this->dataView);

	}

}