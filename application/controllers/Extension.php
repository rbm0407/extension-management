<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Extension extends MY_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Extension_model', 'ext');
	}

        public function api_get($order_by='department.dep_name, employee.emp_name', $order_by_dir='asc')
        {
            $this->dataView['extensions'] = $this->ext->order($order_by, $order_by_dir);
            $this->bladeView('extension_table', $this->dataView);
        }

        public function api_get_dep($returnType='json')
        {
            $this->load->model('Department_model', 'dep');

            $deps  = $this->dep->preorder($this->dep->get_all());

            switch($returnType)
            {
                case 'json':
                default:
                    echo json_encode($deps);
                break;
            }

            
        }
        
        public function api_get_by_dep($dep_id)
        {
            $this->db->where('department.dep_id', $dep_id);
            $this->api_get();
        }
        
        public function api_search()
        {
            if(!$this->input->post() || !$this->input->post('q'))
            {
                return;
            }
            
            $q = $this->input->post('q');
            
            $this->db->like('department.dep_name', $q);
            $this->db->or_like('employee.emp_name', $q);
            $this->db->or_like('extension.ext_num', $q);
            
            $this->api_get();
            
        }
        
	public function index()
	{
		$this->load->model('Department_model', 'dep');

		$this->dataView['departments'] = $this->dep->get_all();
                $this->dataView['extensions'] = $this->ext->with('department')->with('employee')->get_all();
		$this->bladeView('extension_home', $this->dataView);
	}
        
}
