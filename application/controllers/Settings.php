<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends MY_Controller 
{
    
    public function language_change($newLang)
    {
        $this->session->set_userdata('lang', $newLang);
        redirect('/');
    }
    
}