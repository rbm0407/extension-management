<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="utf-8">
	<title>{{ $title }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="theme-color" content="#0c84e4">
	<link rel="stylesheet" href="{{ base_url('assets/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ base_url('assets/css/style.css') }}">
</head>
<body>

<div class="container-fluid">
	@yield('content')
</div>

	<script type="text/javascript" src="{{ base_url('assets/js/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ base_url('assets/js/bootstrap.min.js') }}"></script>
</body>
</html>