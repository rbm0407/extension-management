<h3>{{ lang('list_ext') }}</h3>
@if($extensions)
<table class="table table-responsive table-hover table-bordered">
        <thead>
                <th>{{ lang('dep_singular') }}</th>
                <th>{{ lang('emp_singular') }}</th>
                <th>{{ lang('num_singular') }}</th>
        </thead>
        <tbody>
                @foreach($extensions as $ext)
                <tr>
                        <td>{{ $ext->dep_name }}</td>
                        <td>{{ $ext->emp_name }}</td>
                        <td>{{ $ext->ext_num }}</td>
                </tr>
                @endforeach
        </tbody>
</table>
@else
        <div class="alert alert-info">Doesn't exist any Extension yet</div>
@endif