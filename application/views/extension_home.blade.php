@extends('app')
@section('content')
	<div class="col-md-12">
            <h3>{{ lang('search') }}</h3>
            <form role="form" action="javascript:void(0);">  
                <div class="row">
                  <div class="col-xs-12">
                    <div class="input-group input-group-lg">
                        <input id="search-input" type="text" class="form-control input-focus" name="q" 
                               placeholder="{{ lang('search_input_placeholder') }}" autocomplete="off">
                      <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary glyphicon glyphicon-search"></button>
                      </div><!-- /btn-group -->
                    </div><!-- /input-group -->
                  </div><!-- /.col-xs-12 -->
                </div><!-- /.row -->
            </form>
	</div>
	<div class="col-md-3 col-sm-12">
		<h3>{{ lang('search_dep') }}</h3>
		<form class="form-horizontal" action="javascript:void(0);" method="post" role="form">
			<label for="id_dep">{{ lang('dep_name') }}</label>
			<select id="id_dep" name="id_dep" class="form-control">
                                        <option value="">({{ lang('dep_all') }})</option>
				@foreach($departments as $dep)
					<option value="{{ $dep->dep_id }}">{{ $dep->dep_name }}</option>
				@endforeach
			</select>
			<br>
			<!--<input type="submit" class="btn btn-primary" value="Search">-->
		</form>
	</div>
	<div id="ext-container"  class="col-md-9 col-sm-12">
            <div class="alert alert-info">{{ lang('loading') }}</div>
            <noscript>
                <div class="alert alert-warning">{{ lang('js_required') }}</div>
            </noscript>
	</div>
@endsection