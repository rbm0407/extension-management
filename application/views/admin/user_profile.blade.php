@extends('admin')

@section('content')
    <h2>Hello {{ $userdb->user_name }}</h2>
    <hr>
    <div class="col-md-8 col-md-offset-2">
        <h3>Change User Info</h3>
        <br>
        @if(validation_errors())
            <div class="col-md-12">
                <div class="alert alert-danger">
                    {!! validation_errors() !!}
                    <p>
                        <em>All data are returned to previous version</em>
                    </p>
                </div>
            </div>
        @endif
        <form class="form-horizontal" action="{{ site_url('admin/user/edit') }}" method="post">
            <div class="col-md-12">
                <label for="user_name">User Name:</label>
                <input id="user_name" class="form-control" type="text" name="user_name" required value="{{ $userdb->user_name }}">
                <br>
            </div>
            <div class="col-md-12">
                <label for="user_login">User Login:</label>
                <input id="user_login" class="form-control" type="text" name="user_login" required value="{{ $userdb->user_login }}">
                <br>
            </div>
            <br>
            <div class="col-md-12">
                <label for="user_email">User E-mail:</label>
                <input id="user_email" class="form-control" type="email" name="user_email" required value="{{ $userdb->user_email }}">
                <br>
            </div>
            <br>
            <div class="col-md-12">
                <label for="password">New Password</label>
                <input id="password" class="form-control" type="password" name="new_password">
                <br>
            </div>
            <br>
            <div class="col-md-12">
                <label for="password_confirm">New Password Confirm</label>
                <input id="password_confirm" class="form-control" type="password" name="new_password_confirm">
                <br>
            </div>
            <br>
            <div class="col-md-12">
                <input class="btn btn-primary" type="submit" value="Submit">
            </div>
        </form>
    </div>
@endsection