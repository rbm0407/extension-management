@extends('admin')
@section('content')
	<div class="col-md-6 col-sm-12">
		<h3>Edit Department {{ $department->dep_name }}</h3>
		<form class="form-horizontal" action="{{ site_url('admin/department/edit_post') }}" method="post">
			<input class="form-control input-focus input-selected" type="text" name="dep_name" placeholder="Department Name" value="{{ $department->dep_name }}"><br>
			<input type="submit" class="btn btn-primary" value="Edit">
		</form>
	</div>
@endsection