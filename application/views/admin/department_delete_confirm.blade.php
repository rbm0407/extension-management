@extends('admin')
@section('content')
<div class="col-md-12">
	<div class="alert alert-danger"> Are you sure delete department '{{ $department->dep_name }}'? </div>
	<div class="btn-group">
		<a class="btn btn-danger" href="{{ site_url('admin/department/delete_post') }}">Delete</a>
		<a class="btn btn-primary" href="{{ site_url('admin/department') }}">Cancel</a>
	</div>
</div>
@endsection