@extends('admin')
@section('content')
	<div class="col-md-6 col-sm-12">
		<h3>Edit Employee {{ $employee->emp_name }}</h3>
		<form class="form-horizontal" action="{{ site_url('admin/employee/edit_post') }}" method="post">
			<input class="form-control input-focus input-selected" type="text" name="emp_name" placeholder="Department Name" value="{{ $employee->emp_name }}"><br>
			<input type="submit" class="btn btn-primary" value="Edit">
		</form>
	</div>
@endsection