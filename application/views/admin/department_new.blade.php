@extends('admin')
@section('content')
	<div class="col-md-6 col-sm-12">
		<h3>Create New Department</h3>
		<form class="form-horizontal" action="{{ site_url('admin/department/new_post') }}" method="post">
			<input class="form-control input-focus" type="text" name="dep_name" placeholder="Department Name"><br>
			<input type="submit" class="btn btn-primary" value="Create">
		</form>
	</div>
	<div class="col-md-6 col-sm-12">
		<h3>List of Departments</h3>
		@if($departments)
		<table class="table table-responsive table-hover table-bordered">
			<thead>
				<th>Department</th>
				<th>Actions</th>
			</thead>
			<tbody>
				@foreach($departments as $dep)
				<tr>
					<td class="col-md-10 col-sm-9 col-xs-9">{{ $dep->dep_name }}</td>
					<td class="col-md-2 col-sm-3 col-xs-3">
						<div class="btn-group" role="group">
							<a class="btn btn-warning btn-sm" href="{{ site_url("admin/department/edit/$dep->dep_id") }}"><span class="glyphicon glyphicon-pencil"></a>
							<a class="btn btn-danger btn-sm" href="{{ site_url("admin/department/delete/$dep->dep_id") }}"><span class="glyphicon glyphicon-remove"></a>
						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		@else
			<div class="alert alert-info">Doesn't exist any Department yet</div>
		@endif
	</div>
	
@endsection