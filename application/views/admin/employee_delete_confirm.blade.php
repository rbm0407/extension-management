@extends('admin')
@section('content')
<div class="col-md-12">
	<div class="alert alert-danger"> Are you sure delete employee '{{ $employee->emp_name }}'? </div>
	<div class="btn-group">
		<a class="btn btn-danger" href="{{ site_url('admin/employee/delete_post') }}">Delete</a>
		<a class="btn btn-primary" href="{{ site_url('admin/employee') }}">Cancel</a>
	</div>
</div>
@endsection