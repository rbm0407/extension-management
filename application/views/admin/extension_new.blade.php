@extends('admin')
@section('content')
	<div class="col-md-6 col-sm-12">
		<h3>Create New Extension</h3>
		@if(!$departments || !$employees)
			<div class="alert alert-warning">To create a new extension, you need a department end employee</div>
		@else
			<form class="form-horizontal" action="{{ site_url('admin/extension/new_post') }}" method="post">
				<label for="dep_id">Department</label>
				<select id="dep_id" class="form-control" name="dep_id">
					@foreach($departments as $dep)
						<option value="{{ $dep->dep_id }}">{{ $dep->dep_name }}</option>
					@endforeach
				</select>
				<br>
				<label for="emp_id">Employee</label>
				<select id="emp_id" class="form-control" name="emp_id">
					@foreach($employees as $emp)
						<option value="{{ $emp->emp_id }}">{{ $emp->emp_name }}</option>
					@endforeach
				</select>
				<br>
				<label for="ext_num">Extension Number (Phone Number)</label>
				<input id="ext_num" class="form-control" type="text" name="ext_num" placeholder="E.g: 777 or 3222-5555"><br>
				<input type="submit" class="btn btn-primary" value="Create">
			</form>
		@endif
	</div>
	<div class="col-md-6 col-sm-12">
		<h3>List of Extensions</h3>
		@if($extensions)
		<table class="table table-responsive table-hover table-bordered">
			<thead>
				<th>Department</th>
				<th>Employee</th>
				<th>Number</th>
				<th>Actions</th>
			</thead>
			<tbody>
				@foreach($extensions as $ext)
				<tr>
					<td>{{ $ext->department->dep_name }}</td>
					<td>{{ $ext->employee->emp_name }}</td>
					<td>{{ $ext->ext_num }}</td>
					<td class="col-md-2 col-sm-3 col-xs-3">
						<div class="btn-group" role="group">
							<a class="btn btn-warning btn-sm" href="{{ site_url("admin/extension/edit/$ext->ext_id") }}"><span class="glyphicon glyphicon-pencil"></a>
							<a class="btn btn-danger btn-sm" href="{{ site_url("admin/extension/delete/$ext->ext_id") }}"><span class="glyphicon glyphicon-remove"></a>
						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		@else
			<div class="alert alert-info">Doesn't exist any Extension yet</div>
		@endif
	</div>
	
@endsection