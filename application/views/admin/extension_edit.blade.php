@extends('admin')
@section('content')
	<div class="col-md-6 col-sm-12">
		<h3>Edit Extension {{ $ext->ext_num }}</h3>
		@if(!$departments || !$employees)
			<div class="alert alert-warning">To edit this extension, you need a department end employee</div>
		@else
			<form class="form-horizontal" action="{{ site_url('admin/extension/edit_post') }}" method="post">
				<label for="dep_id">Department</label>
				<select id="dep_id" class="form-control" name="dep_id">
					@foreach($departments as $dep)
						<option value="{{ $dep->dep_id }}" @if($dep->dep_id == $ext->dep_id) selected="selected" @endif>{{ $dep->dep_name }}</option>
					@endforeach
				</select>
				<br>
				<label for="emp_id">Employee</label>
				<select id="emp_id" class="form-control" name="emp_id">
					@foreach($employees as $emp)
						<option value="{{ $emp->emp_id }}" @if($emp->emp_id == $ext->emp_id) selected="selected" @endif>{{ $emp->emp_name }}</option>
					@endforeach
				</select>
				<br>
				<label for="ext_num">Extension Number (Phone Number)</label>
				<input id="ext_num" class="form-control" type="text" name="ext_num" placeholder="E.g: 777 or 3222-5555" value="{{ $ext->ext_num }}"><br>
				<input type="submit" class="btn btn-primary" value="Edit">
			</form>
		@endif
	</div>
@endsection