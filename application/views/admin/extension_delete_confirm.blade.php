@extends('admin')
@section('content')
<div class="col-md-12">
	<div class="alert alert-danger"> Are you sure delete extension '{{ $extension->ext_num }}'? </div>
	<div class="btn-group">
		<a class="btn btn-danger" href="{{ site_url('admin/extension/delete_post') }}">Delete</a>
		<a class="btn btn-primary" href="{{ site_url('admin/extension') }}">Cancel</a>
	</div>
</div>
@endsection