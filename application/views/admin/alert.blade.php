@extends('admin')
@section('content')
	<div class="col-md-12">
		<div class="alert alert-{{$alertType}}">
			{{ $message }}
		</div>
	</div>
@endsection