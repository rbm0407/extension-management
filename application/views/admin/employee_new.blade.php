@extends('admin')
@section('content')
	<div class="col-md-6 col-sm-12">
		<h3>Create New Employee</h3>
		<form class="form-horizontal" action="{{ site_url('admin/employee/new_post') }}" method="post">
			<input class="form-control input-focus" type="text" name="emp_name" placeholder="Employee Name"><br>
			<input type="submit" class="btn btn-primary" value="Create">
		</form>
	</div>
	<div class="col-md-6 col-sm-12">
		<h3>List of Employees</h3>
		@if($employees)
		<table class="table table-responsive table-hover table-bordered">
			<thead>
				<th>Department</th>
				<th>Actions</th>
			</thead>
			<tbody>
				@foreach($employees as $emp)
				<tr>
					<td class="col-md-10 col-sm-9 col-xs-9">{{ $emp->emp_name }}</td>
					<td class="col-md-2 col-sm-3 col-xs-3">
						<div class="btn-group" role="group">
							<a class="btn btn-warning btn-sm" href="{{ site_url("admin/employee/edit/$emp->emp_id") }}"><span class="glyphicon glyphicon-pencil"></a>
							<a class="btn btn-danger btn-sm" href="{{ site_url("admin/employee/delete/$emp->emp_id") }}"><span class="glyphicon glyphicon-remove"></a>
						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		@else
			<div class="alert alert-info">Doesn't exist any Employee yet</div>
		@endif
	</div>
	
@endsection