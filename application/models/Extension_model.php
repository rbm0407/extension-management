<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Extension_model extends MY_Model
{

	public $_table = 'extension';
	public $primary_key = 'ext_id';
	public $belongs_to = array(
		'department' => array('model' => 'department_model', 'primary_key' => 'dep_id'), 
		'employee' => array('model' => 'employee_model', 'primary_key' => 'emp_id')
	);
        public $before_get = array('preorder');

        public function preorder($db){
            $this->db->order_by('ext_num', 'ASC');
            return $db;
        }
        
        
        public function joins()
        {
            $this->db->join('department', 'department.dep_id= extension.dep_id');
            $this->db->join('employee', 'employee.emp_id= extension.emp_id');
        }
        
        public function order($order_by, $direction='ASC', $joins=TRUE)
        {
            
            if($joins)
            {
                $this->joins();
            }
            
            $this->db->order_by($order_by, $direction);
            
            return $this->db->get('extension')->result();
            
        }
        
        

}