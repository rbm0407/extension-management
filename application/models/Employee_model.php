<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Employee_model extends MY_Model {

	public $_table = 'employee';
	public $primary_key = 'emp_id';
        public $before_get = array('preorder');

        public function preorder($db){
            $this->db->order_by('emp_name', 'ASC');
            return $db;
        }
        


}