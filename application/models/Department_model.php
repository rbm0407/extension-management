<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Department_model extends MY_Model {

	public $_table = 'department';
	public $primary_key = 'dep_id';
        public $before_get = array('preorder');

        public function preorder($db)
        {
            $this->db->order_by('dep_name', 'ASC');
            return $db;
        }
        

}