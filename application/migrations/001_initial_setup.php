<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Initial_Setup extends CI_Migration
{
    
    private function timestamps()
    {
        $this->dbforge->add_field(array(
            'created_at' => array(
                'type' => 'DATETIME',
            ),
            'updated_at' => array(
                'type' => 'DATETIME',
                'null' => TRUE
            )
        ));
    }

    public function up()
    {

        $this->dbforge->add_field(array(
            'ext_id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'dep_id' => array(
                'type' => 'INT',
                'constraint' => 5
            ),
            'emp_id' => array(
                'type' => 'INT',
                'constraint' => 5
            ),
            'ext_num' => array(
                'type' => 'VARCHAR',
                'constraint' => 20
            ),
        ));

        $this->dbforge->add_key('ext_id', TRUE);
        $this->dbforge->create_table('extension');

        $this->dbforge->add_field(array(
            'dep_id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'dep_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 150
            ),
        ));


        $this->dbforge->add_key('dep_id', TRUE);
        $this->dbforge->create_table('department');


        $this->dbforge->add_field(array(
            'emp_id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'emp_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 150
            ),
        ));

        $this->dbforge->add_key('emp_id', TRUE);
        $this->dbforge->create_table('employee');

        $this->dbforge->add_field(array(
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'user_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 80
            ),
            'user_login' => array(
                'type' => 'VARCHAR',
                'constraint' => 80
            ),
            'user_email' => array(
                'type' => 'VARCHAR',
                'constraint' => 80
            ),
            'user_password' => array(
                'type' => 'VARCHAR',
                'constraint' => 255
            )
        ));

        $this->timestamps();

        $this->dbforge->add_key('user_id', TRUE);
        $this->dbforge->create_table('user');

        $this->db->set('created_at', 'NOW()', FALSE);
        $this->db->insert('user',
            array(
            'user_name' => 'Admin',
            'user_login' => 'admin',
            'user_email' => 'admin@example.com',
            'user_password' => password_hash('password', PASSWORD_DEFAULT)
            )
        );
    }

    public function down()
    {
        $this->dbforge->drop_table('employee');
        $this->dbforge->drop_table('department');
        $this->dbforge->drop_table('extension');
        $this->dbforge->drop_table('user');
    }
}