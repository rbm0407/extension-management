############################
Extension Number Management
############################

Made with [CodeIgniter](http://www.codeigniter.com) and [Blade Templating](http://laravel.com/docs/5.0/templates) ([forked to work with CodeIgniter](https://bitbucket.org/rbm0407/ciblade))

*************
Installation 
*************

Copy this project to your host, edit the application/config/database.php file to your database credentials and
execute migrate controller to import the database.

Change the permissions of folder application/cache to writable (ex: 775)

**************************
Changelog and New Features
**************************

Admin Page and Front Page created
User Admin Editor created


*******************
Server Requirements
*******************

PHP version 5.4 or newer is recommended.

It should work on 5.2.4 as well, but we strongly advise you NOT to run
such old versions of PHP, because of potential security and performance
issues, as well as missing features.

*******
License
*******

[GNU General Public License, version 2] (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

*********
Resources
*********

-  `[CodeIgniter User Guide] (http://www.codeigniter.com/docs)`_
-  `[Blade Template] (http://laravel.com/docs/5.0/templates)`_

Report security issues to our [Issues Panel] (https://bitbucket.org/rbm0407/extension-management/issues)`.
