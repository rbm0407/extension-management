$('document').ready(function(){
    
    get_init();
    
    $('#id_dep').change(function(){
        
        var id_dep = $(this).val();
        
        $('#search-input').val('');
        
        if(id_dep===''){
            get_init();
        } else {
            $.get(site_url+ '/extension/api_get_by_dep/'+ id_dep, function(data){
                $('#ext-container').html(data);
            });
        }
        
    });
    
    $('#search-input').keyup(function(){
        var q = $(this).val();
        
        if(q===''){
            get_init();
        }
        
        $('#id_dep option:first-child').attr('selected', 'selected');
        search(q);
    });
    
    
});


function get_init(){
    $.get(site_url + '/extension/api_get', function(data){
        $('#ext-container').html(data);
    });
}

function search(q){
    $.post(site_url + '/extension/api_search', { q: q }, function(data){
        $('#ext-container').html(data);
    });
}